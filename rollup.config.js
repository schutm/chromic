import buble from 'rollup-plugin-buble';
import includePaths from 'rollup-plugin-includepaths';

/* global process: false */
const es5 = process.env.es5 === 'true';
const isAmd = process.env.format === 'amd';
const moduleName = process.env.name;

const config = {
	output: {
		format: 'es'
	},
	external: [
		'ava'
	],
	plugins: [
		es5 ? buble({}) : {},
		includePaths({ paths: ['./', '../output/deps'] })
	]
};

if (isAmd) {
	config.amd = { id: moduleName || 'chromic' };
} else {
	config.name = moduleName || 'chromic';
}

export default config;
