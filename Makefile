DESTDIR ?= ../output
DISTDIR ?= $(DESTDIR)/dist
DEPSDIR := $(DESTDIR)/deps
TESTDIR ?= $(DESTDIR)/tests
REPORTDIR ?= $(DESTDIR)/reports
SRCDIR ?= src

DEPS := macgyver
DEPS_macgyver := https://gitlab.com/schutm/macgyver/repository/archive.tar.bz2?ref=master

SRCS := $(patsubst src/%,%,$(wildcard src/*.js))
LIBS := $(patsubst src/%,%,$(wildcard src/**/*.js))
TESTS := $(patsubst tests/%,%,$(wildcard tests/*.js $(wildcard tests/**/*.js)))

FORMATS := amd cjs es iife umd

NODE := NODE_PATH=$(shell npm root -g 2> /dev/null)
GET := $(shell which curl && echo "curl -ssL" || echo "wget -qO-")

dependencies: $(addprefix $(DEPSDIR)/,$(DEPS))

.SUFFIXES:
.PHONY: clean dist lint test test-coverage

dist: $(foreach FORMAT,$(FORMATS),$(addprefix \
		$(DISTDIR)/$(FORMAT)/,\
		$(foreach EXT,js min.js es5.js es5.min.js,$(patsubst %.js,%.$(EXT),$(SRCS)))\
	))

clean:
	-@rm -rf $(DESTDIR)
	-@rm -rf $(DISTDIR)

lint:
	@$(NODE) eslint '**/*.js'

lint-ci: | $(abspath $(REPORTDIR)/lint)/
	+@echo "Linting"
	@$(NODE) eslint -f html -o $(abspath $(REPORTDIR)/lint/index.html) '**/*.js' || true
	@$(NODE) script $(abspath $(REPORTDIR)/lint.out) -qec "eslint '**/*.js'"

test: test-prepare
	@cd $(TESTDIR) && $(NODE) ava 'tests/**/*.js'

test-coverage: test-prepare
	@cd $(TESTDIR) && $(NODE) nyc --include 'tests/lib/**/*.js' --reporter=text ava 'tests/**/*.js'

test-ci: test-prepare | $(abspath $(REPORTDIR)/coverage)/ $(abspath $(REPORTDIR)/tests)/
	+@echo "Executing tests"
	@cd $(TESTDIR) && $(NODE) nyc --silent --include 'tests/lib/**/*.js' --all ava -t 'tests/**/*.js' > $(abspath $(REPORTDIR)/tests.tap) || true
	@cd $(TESTDIR) && $(NODE) cat $(abspath $(REPORTDIR)/tests.tap) | tap-that-html > $(abspath $(REPORTDIR)/tests/index.html)
	@cd $(TESTDIR) && $(NODE) cat $(abspath $(REPORTDIR)/tests.tap) | tap-diff
	@cd $(TESTDIR) && $(NODE) nyc report --include 'tests/lib/**/*.js' --reporter=html --report-dir=$(abspath $(REPORTDIR)/coverage/)
	@cd $(TESTDIR) && $(NODE) script $(abspath $(REPORTDIR)/coverage.out) -qec "nyc report --include 'tests/lib/**/*.js' --reporter=text-summary"

test-prepare: $(TESTDIR)/deps/ $(patsubst %.js,$(TESTDIR)/src/%.js,$(SRCS) $(LIBS)) $(addprefix $(TESTDIR)/tests/,$(TESTS))

test-dependencies: dependencies $(TESTDIR)/src/

%/:
	@mkdir -p $@

$(TESTDIR)/deps/: | $(DEPSDIR)/ $(TESTDIR)/
	ln -sr $(DEPSDIR) $(TESTDIR)/deps

define DISTBUILD_template =
$(DISTDIR)/$(1)/%.js: src/%.js | dependencies
	+@echo "Creating $$< => $$@"
	@$(NODE) rollup -c rollup.config.js -f $(1) -m --no-conflict -i $$< -o $$@ --environment format:$(1),name:$$*

$(DISTDIR)/$(1)/%.es5.js: src/%.js | dependencies
	+@echo "Transpiling $$< => $$@"
	@$(NODE) rollup -c rollup.config.js -f $(1) -m --no-conflict -i $$< -o $$@ --environment es5,format:$(1),name:$$*

$(DISTDIR)/$(1)/%.min.js: $(DISTDIR)/$(1)/%.js
	+@echo "Minifying $$< => $$@"
	@$(NODE) uglifyjs -o $$@ -c -m --source-map filename=\"$$@.map\",content=\"$$<.map\" -- $$<
endef

$(foreach format,$(FORMATS),\
	$(eval $(call DISTBUILD_template,$(format)))\
)

.SECONDEXPANSION:
$(DEPSDIR)/%:
	+@echo "Downloading dependency $@ from $(DEPS_$*)"
	@mkdir -p $@
	@cd $@ && $(GET) $(DEPS_$*) | tar xj --wildcards --strip-components=2 '*/src/*'

$(TESTDIR)/src/%.js: src/%.js | test-dependencies
	+@echo "Transforming source file to test $< => $@"
	@$(NODE) rollup -c -f umd -m inline --no-conflict -i $< -o $@

$(TESTDIR)/tests/%.js: tests/%.js
	+@echo "Copying file $< => $@"
	@mkdir -p $(dir $@) && cp $< $@
