project     = ENV.fetch('PROJECT', File.basename(File.dirname(__FILE__)))

base_dir    = ENV.fetch('BASEDIR', File.expand_path(File.dirname(__FILE__) + '/../..'))
source_dir  = ENV.fetch('SOURCEDIR', File.dirname(__FILE__))
output_dir  = ENV.fetch('OUTPUTDIR', "#{base_dir}/Outputs/#{project}")

def ensure_dir target
  unless File.directory?(target)
    FileUtils.mkdir_p(target)
  end
end

def provision root
  Dir.glob("#{root}/**/*.provisioner").sort.each do |f|
    puts "Including '#{f}'"
    Vagrant.configure(2) do |config|
      config.vm.provision "shell", path: f
    end
  end
end

ensure_dir(output_dir)

Vagrant.configure(2) do |config|
  # Box to work with
  config.vm.box = "debian/contrib-jessie64"

  # Shared folders.
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.synced_folder source_dir, "/project/sources"

  # Set owner of shared folder.
  config.vm.provision "shell", inline: "chown vagrant:vagrant /project"

  # Make sure we don't get non-interactive shell problems
  config.ssh.shell = "bash"
end

provision(source_dir)
