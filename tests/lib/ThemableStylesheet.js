import test from 'ava';
import ThemableStylesheet from '../../src/lib/ThemableStylesheet';

const stylesheet1 = (theme) => `
  .stylesheet {
    background-color: ${theme.backgroundColor};
    color: ${theme.color};
  }`;

const stylesheet2 = (theme) => `
  .stylesheet {
    background-color: ${theme.backgroundColor};
    color: ${theme.color};
  }`;

const themeA = {
	backgroundColor: 'red',
	color: 'white'
};

const themeB = {
	backgroundColor: 'red',
	color: 'white'
};

test('Correct css', (t) => {
	const output = `
  .stylesheet {
    background-color: red;
    color: white;
  }`;

	const themableStylesheet1 = new ThemableStylesheet(stylesheet1);
	const result = themableStylesheet1.getThemedStylesheet(themeA);

	t.deepEqual(result.unscopedCss, output);
});

test('Construction', (t) => {
	t.is(new ThemableStylesheet(stylesheet1), new ThemableStylesheet(stylesheet1), 'Construction from same stylesheet returns single ThemableStylesheet');
	t.not(new ThemableStylesheet(stylesheet1), new ThemableStylesheet(stylesheet2), 'Construction from different stylesheets return different ThemableStylesheets');
});

test('Created stylesheets', (t) => {
	const themableStylesheet1 = new ThemableStylesheet(stylesheet1);
	const themableStylesheet2 = new ThemableStylesheet(stylesheet2);

	t.is(themableStylesheet1.getThemedStylesheet(themeA), themableStylesheet1.getThemedStylesheet(themeA), 'Same stylesheet and theme return single ThemedStylesheet');
	t.not(themableStylesheet1.getThemedStylesheet(themeA), themableStylesheet1.getThemedStylesheet(themeB), 'Same stylesheet and different theme return different ThemedStylesheets');
	t.not(themableStylesheet1.getThemedStylesheet(themeA), themableStylesheet2.getThemedStylesheet(themeA), 'Different stylesheet and theme return different ThemedStylesheets');
	t.not(themableStylesheet1.getThemedStylesheet(themeA), themableStylesheet2.getThemedStylesheet(themeB), 'Different stylesheet and different theme return different ThemedStylesheets');
});
