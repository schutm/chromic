/**
 * Tests based on csjs module https://github.com/rtsao/csjs/
 */
import test from 'ava';
import ThemedStylesheet from '../../src/lib/ThemedStylesheet';

function mapCompositionsToSimpleObject(obj) {
	return Object.keys(obj).reduce((acc, key) => Object.assign({}, acc, { [key]: `${obj[key]}` }), {});
}

function performAssertion(t, { input, keyframes, classes, output }) {
	const result = new ThemedStylesheet(input);

	t.is(result.unscopedCss, input, 'Unscoped CSS equals input');
	t.deepEqual(mapCompositionsToSimpleObject(result.keyframes), keyframes, 'Keyframe mapping is not correct');
	t.deepEqual(mapCompositionsToSimpleObject(result.classes), classes, 'Class mapping is not correct');
	t.is(result.css, output, 'Scoped CSS equals output');
	t.is(result.toString(), output, 'Explicit toString() invocation');
	t.is(`${result}`, output, 'Implicit toString() invocation');
}

test('Basic scoping', (t) => {
	const input = `
		.foo {
			color: red;
		}`;
	const output = `
		.foo_3PavUf {
			color: red;
		}`;

	performAssertion(t, {
		input,
		output,
		keyframes: {},
		classes: { foo: 'foo_3PavUf' }
	});
});

test('Animations with pseudo-selector names are not scoped', (t) => {
	const input = `
		@keyframes hover {
			0%   { opacity: 0.0; }
			100% { opacity: 0.5; }
		}
		@media (max-width: 480px) {
			.animation:hover {
				background: green;
			}
		}`;
	const output = `
		@keyframes hover_3i0ZxA {
			0%   { opacity: 0.0; }
			100% { opacity: 0.5; }
		}
		@media (max-width: 480px) {
			.animation_3i0ZxA:hover {
				background: green;
			}
		}`;

	performAssertion(t, {
		input,
		output,
		keyframes: { hover: 'hover_3i0ZxA' },
		classes: { animation: 'animation_3i0ZxA' }
	});
});

test('Comments are left untouched, including selectors', (t) => {
	const input = `
		/* Here's a comment */
		/*squishedcomment*/
		/********crazycomment********/
		/* A comment with a period at the end. */
		/* A comment with a period followed by numbers v1.2.3 */
		/* A comment with a .className */
		/*
		 * Here's a comment
		 * A comment with a period at the end.
		 * A comment with a period followed by numbers v1.2.3
		 * A comment with a .className
		 */
		/* .inlineCss { color: red; } */
		/*
		.commentedOutCss {
			color: blue;
		}
		*/
		.foo {
			color: red; /* comment on a line */
			animation-name: wow;
		}
		@keyframes wow {}
		/*
		@keyframes bam {}
		 */

		/* .woot {
			animation-name: bam;
		} */
		/* .hmm {
			animation-name: wow;
		} */`;
	const output = `
		/* Here's a comment */
		/*squishedcomment*/
		/********crazycomment********/
		/* A comment with a period at the end. */
		/* A comment with a period followed by numbers v1.2.3 */
		/* A comment with a .className */
		/*
		 * Here's a comment
		 * A comment with a period at the end.
		 * A comment with a period followed by numbers v1.2.3
		 * A comment with a .className
		 */
		/* .inlineCss { color: red; } */
		/*
		.commentedOutCss {
			color: blue;
		}
		*/
		.foo_3Elrg {
			color: red; /* comment on a line */
			animation-name: wow_3Elrg;
		}
		@keyframes wow_3Elrg {}
		/*
		@keyframes bam {}
		 */

		/* .woot {
			animation-name: bam;
		} */
		/* .hmm {
			animation-name: wow;
		} */`;

	performAssertion(t, {
		input,
		output,
		keyframes: { wow: 'wow_3Elrg' },
		classes: { foo: 'foo_3Elrg' }
	});
});

test('Dots in values are not confused with classes', (t) => {
	const input = `
		.foo {
			font-size: 1.3em;
		}

		.bar { font-size: 12.5px; } .baz { width: 33.3% }`;
	const output = `
		.foo_3n2uGc {
			font-size: 1.3em;
		}

		.bar_3n2uGc { font-size: 12.5px; } .baz_3n2uGc { width: 33.3% }`;

	performAssertion(t, {
		input,
		output,
		keyframes: {},
		classes: {
			foo: 'foo_3n2uGc',
			bar: 'bar_3n2uGc',
			baz: 'baz_3n2uGc'
		}
	});
});

test('Keyframes with decimal are not confused with classes', (t) => {
	const input = `
		@keyframes woot {
			0%   { opacity: 0; }
			33.3% { opacity: 0.333; }
			100% { opacity: 1; }
		}`;
	const output = `
		@keyframes woot_38VwmF {
			0%   { opacity: 0; }
			33.3% { opacity: 0.333; }
			100% { opacity: 1; }
		}`;

	performAssertion(t, {
		input,
		output,
		keyframes: { woot: 'woot_38VwmF' },
		classes: {}
	});
});

test('Media-queries are scoped', (t) => {
	const input = `
		.foo {
			color: red;
		}

		@media (max-width: 480px) {
			.foo {
				color: blue;
			}
		}`;
	const output = `
		.foo_159uOV {
			color: red;
		}

		@media (max-width: 480px) {
			.foo_159uOV {
				color: blue;
			}
		}`;

	performAssertion(t, {
		input,
		output,
		keyframes: {},
		classes: { foo: 'foo_159uOV' }
	});
});

test('Non-classes are not scoped', (t) => {
	const input = `
		#foo {
			color: red;
		}`;
	const output = `
		#foo {
			color: red;
		}`;

	performAssertion(t, {
		input,
		output,
		keyframes: {},
		classes: {}
	});
});

test('Not-pseudo selector is scoped', (t) => {
	const input = `
		@media screen and (min-width: 769px) {
			.foo:not(.bar) {
				display: flex;
			}
		}`;
	const output = `
		@media screen and (min-width: 769px) {
			.foo_3aFrLD:not(.bar_3aFrLD) {
				display: flex;
			}
		}`;

	performAssertion(t, {
		input,
		output,
		keyframes: {},
		classes: {
			foo: 'foo_3aFrLD',
			bar: 'bar_3aFrLD'
		}
	});
});

test('Translating scoped classnames to unscoped', (t) => {
	const style = new ThemedStylesheet(`.foo { color: red; } .bar { color: green; }`);
	const classes = style.classes;

	t.is(classes('foo'), '.foo_4k3Auy', 'Unscoped classname translates to scoped classname');
	t.is(classes('selector'), '.selector', 'Unknown unscoped classname is kept');
	t.is(classes(null), '', 'Undefined classname is removed');

	// eslint-disable-next-line no-undefined
	t.is(classes('foo', null, 'bar', undefined, 'selector'), '.foo_4k3Auy .bar_4k3Auy .selector', 'Array translated correct');
});
