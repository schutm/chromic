import test from 'ava';
import Composition from '../../src/lib/Composition';

test('Selector returns new scoped CSS selector', (t) => {
	const composition = new Composition('bar_foo', 'bar', '.');

	t.is(composition.selector, '.bar_foo');
});

test('UnscopedSelector returns original CSS selector', (t) => {
	const composition = new Composition('bar_foo', 'bar', '.');

	t.is(composition.unscopedSelector, '.bar');
});

test('toString returns new scoped class name', (t) => {
	const composition = new Composition('bar_foo', 'bar', '.');

	t.is(composition.toString(), 'bar_foo', 'Explicit toString() invocation');
	t.is(`${composition}`, 'bar_foo', 'Implicit toString() invocation');
});
