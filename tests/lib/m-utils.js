import test from 'ava';
import { wrapComponent } from '../../src/lib/m-utils';

class Component {
	view(arg) { return `(view) arg: ${arg}`; }
	unknown(arg) { return `(unknown) arg: ${arg}`; }
}

test('Mithril methods are wrapped', (t) => {
	const WrappedComponent = wrapComponent(
		Component,
		(fn) => (...args) => `[wrapped] ${fn(...args)}`);
	const wrappedComponent = new WrappedComponent();

	t.is(wrappedComponent.view('test'), '[wrapped] (view) arg: test', 'Known method should return wrapped result');
});

test('Non-mithril methods are not wrapped', (t) => {
	const WrappedComponent = wrapComponent(
		Component,
		(fn) => (...args) => `[wrapped] ${fn(...args)}`);
	const wrappedComponent = new WrappedComponent();

	t.is(wrappedComponent.unknown('test'), '(unknown) arg: test', 'Should return non-wrapped result');
});

test('Non-existing mithril methods are not created', (t) => {
	const WrappedComponent = wrapComponent(
		Component,
		(fn) => (...args) => `[wrapped] ${fn(...args)}`);
	const wrappedComponent = new WrappedComponent();

	t.falsy(wrappedComponent.oninit, 'Non-existing method should not exist');
});

test('Explicit set methods to be wrapped', (t) => {
	const WrappedComponent = wrapComponent(
		Component,
		(fn) => (...args) => `[wrapped] ${fn(...args)}`,
		['oninit', 'unknown']);
	const wrappedComponent = new WrappedComponent();

	t.is(wrappedComponent.view('test'), '(view) arg: test', 'Non-passed method should return non-wrapped result');
	t.is(wrappedComponent.unknown('test'), '[wrapped] (unknown) arg: test', 'Passed method should return wrapped result');
	t.falsy(wrappedComponent.oninit, 'Passed, Non-existing method should not exist');
});
