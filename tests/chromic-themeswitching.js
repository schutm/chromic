import test from 'ava';
import { withStyles, activateTheme, deactivateTheme } from '../src/chromic';

class Component {
	view(styles) { return styles.appbar.toString(); }
}

const stylesheet = (theme) => `
  .appbar {
    background-color: ${theme.backgroundColor};
    color: ${theme.color};
  }`;

const defaultSkin = ({ palette }) => {
	return {
		backgroundColor: palette.primary1Color,
		color: palette.alternateTextColor
	};
};

const defaultTheme = {
	palette: {
		primary1Color: '#800000',
		alternateTextColor: 'black'
	}
};

const secondTheme = {
	palette: {
		primary1Color: 'black',
		alternateTextColor: '#800000'
	}
};

test('Theme switching', (t) => {
	const StyledComponent = withStyles(stylesheet, defaultSkin)(Component);
	const styledComponent = new StyledComponent();

	activateTheme(defaultTheme);
	t.is(styledComponent.view(), 'appbar_3xV7Ku');
	activateTheme(secondTheme);
	t.is(styledComponent.view(), 'appbar_SbU6G');
	deactivateTheme();
	t.is(styledComponent.view(), 'appbar_3xV7Ku');
});
