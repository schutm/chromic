import test from 'ava';
import { withStyles, activateTheme } from '../src/chromic';

class Component {
	view(styles) { return styles.appbar.toString(); }
}

const stylesheet = (theme) => `
  .appbar {
    background-color: ${theme.backgroundColor};
    color: ${theme.color};
  }`;

const defaultSkin = ({ palette }) => {
	return {
		backgroundColor: palette.primary1Color,
		color: palette.alternateTextColor
	};
};

const defaultTheme = {
	palette: {
		primary1Color: '#800000',
		alternateTextColor: 'black'
	}
};

test('Simple theming', (t) => {
	const StyledComponent = withStyles(stylesheet, defaultSkin)(Component);
	const styledComponent = new StyledComponent();

	activateTheme(defaultTheme);
	t.is(styledComponent.view(), 'appbar_3xV7Ku');
});
