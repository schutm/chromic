/*
 * The css scoper implementation is based on csjs module:
 * https://github.com/rtsao/csjs/
 * Changes: classified and more es6 functions usage, split collection of class
 *          and keyframe names and replacement into separate steps (resulting
 *          in slower, but cleaner code), removed extends functions (use post
 *          processor for this), no hidden css key among the classes anymore
 *          due to the class object being returned), and more. So basically the
 *          idea and the regexes are the same and everything else has been
 *          adjusted.
 *
 */

import { base62 as encode } from 'macgyver/encoding';
import { djb2 as hash } from 'macgyver/hash';
import { matchAll } from 'macgyver/regex';
import Composition from './Composition';

const classRegex = /(\.)(?!\d)([^\s.,{[>+~#:)]*)(?![^{]*})/.source;
const keyframesRegex = /(@\S*keyframes\s*)([^{\s]*)/.source;
const ignoreCommentsRegex = /(?!(?:[^*/]|\*[^/]|\/[^*])*\*+\/)/.source;

const classMatcher = new RegExp(classRegex + ignoreCommentsRegex, 'g');
const keyframeMatcher = new RegExp(keyframesRegex + ignoreCommentsRegex, 'g');

function scoper(src) {
	const suffix = encode(hash(src));

	return function scopedName(scopeName) {
		return `${scopeName}_${suffix}`;
	};
}

function createCompositions(matcher, unscopedCss, makeScopedName) {
	const matches = matchAll(matcher, unscopedCss);
	const compositions = matches.reduce((accumulator, [_fullMatch, prefix, unscopedName]) => {
		const scopedName = makeScopedName(unscopedName);
		const composition = new Composition(scopedName, unscopedName, prefix);

		return Object.assign((...unscopedClassNames) =>
			unscopedClassNames.filter((unscopedClassName) => !!unscopedClassName)
			                  .map((unscopedClassName) => `.${compositions[unscopedClassName] || unscopedClassName}`)
			                  .join(' '), accumulator, { [unscopedName]: composition }
		);
	}, {});

	return compositions;
}

function createClassCompositions(unscopedCss, makeScopedName) {
	return createCompositions(classMatcher, unscopedCss, makeScopedName);
}

function createKeyframeCompositions(unscopedCss, makeScopedName) {
	return createCompositions(keyframeMatcher, unscopedCss, makeScopedName);
}

function replaceCompositions(matcher, unscopedCss, compositions) {
	return unscopedCss.replace(matcher, (_fullMatch, _prefix, unscopedName) =>
		compositions[unscopedName].selector
	);
}

function replaceAnimations(unscopedCss, keyframeCompositions) {
	const keyframes = Object.keys(keyframeCompositions);
	const animationRegex = '((?:animation|animation-name)\\s*:[^};]*)('
	                     + `${keyframes.join('|')})([;\\s])${ignoreCommentsRegex}`;
	const animationMatcher = new RegExp(animationRegex, 'g');

	return unscopedCss.replace(animationMatcher, (_fullMatch, preamble, unscopedName, ending) =>
		preamble + keyframeCompositions[unscopedName] + ending
	);
}

function scopifyClasses(unscopedCss, classCompositions) {
	return replaceCompositions(classMatcher, unscopedCss, classCompositions);
}

function scopifyKeyframes(unscopedCss, keyframeCompositions) {
	const keyframeScopedCss = replaceCompositions(keyframeMatcher, unscopedCss, keyframeCompositions);
	const scopedCss = replaceAnimations(keyframeScopedCss, keyframeCompositions);

	return scopedCss;
}

function scopify(unscopedCss, classCompositions, keyframeCompositions) {
	const classScopedCss = scopifyClasses(unscopedCss, classCompositions);
	const keyframeScopedCss = scopifyKeyframes(classScopedCss, keyframeCompositions);

	return keyframeScopedCss;
}

export default class ThemedStylesheet {
	constructor(unscopedCss) {
		const makeScopedName = scoper(unscopedCss);

		this.unscopedCss = unscopedCss;
		this.classes = createClassCompositions(unscopedCss, makeScopedName);
		this.keyframes = createKeyframeCompositions(unscopedCss, makeScopedName);
		this.css = scopify(unscopedCss, this.classes, this.keyframes);
	}

	toString() {
		return this.css;
	}
}
